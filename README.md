# Test

Test description
<pre>
{
    "name": "...",
    "authors": [
        {
            "name": "...",
            "email": "...@..."
        }
    ],
    "description": "...",
    "type": "project",
    "require": {
        "pachverk/test": "master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/Pachverk/test.git"
        }
    ]
}
</pre>